
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.core import serializers
import json
from itertools import chain
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.core.mail import send_mail, BadHeaderError
from django.conf import settings

from .models import Top_image_icon # this file for image models
from .models import Middle_image_icon # this file for text models
from .models import Buttom_image_icon
from .models import Text_button
from .models import Blog_page
from .models import Home_index_page1
from .models import Home_index_page2
from .models import Home_index_page3
from .models import Home_index_page4
from .models import Home_middle_page1, Home_middle_page2, Home_middle_page3
from .forms	import Comment_Form1, Comment_Form2
from .forms import Comment_Form3, Comment_Form4 #this file import for forms.py 
from .forms import Blog_Form
from .forms import Index_Form1, Index_Form2
from .forms import Index_Form3, Index_Form4
from .forms import Index_Form_middle, Index_Form_middle1, Index_Form_middle2, Header_Icon_Model
from .models import SEO_Model, Header_Icon_Model
from .forms import Seo_form, Header_Icon



# importing models
from .models import ServicesHeaderModel, ServicesGamePlanModel, ServicesFeaturesModel, ServicesOurServicesModel, ServicesPortfolioModel
from .models import ContactUsModel
from .models import PortfolioWebsiteModel, PortfolioMobileModel, PortfolioCategoryModel, ArticleModel, CopyRightModel
from .models import BlogModel, BlogCategoryModel

# importing forms
from .forms import ServicesHeaderModelForm, ServicesGamePlanModelForm, ServicesFeaturesModelForm 
from .forms import ServicesOurServicesModelForm, ServicesPortfolioModelForm
from .forms import ContactUsModelForm, PortfolioWebsiteModelForm, PortfolioMobileModelForm, PortfolioCategoryModelForm, ArticleModelForm, CopyRightModelForm
from .forms import BlogModelForm, BlogCategoryModelForm




#--Prashant Model-------
from .models import About_us_HeaderImg
from .models import About_us_Body,About_us_BodyImg
from .models import About_us_Img,Portfolio_Page
from .models import Contact_Footernew

#-----Admin Form-----------
from .forms import About_us_HeaderImg_Form
from .forms import About_us_Body_Form
from .forms import About_us_Img_Form
from .forms import About_us_BodyImg_Form
from .forms import Portfolio_Page_Form
from .forms import Contact_Footer_Form

def services_view(request):
	instance = ServicesHeaderModel.objects.all()
	instance2 = ServicesGamePlanModel.objects.all()
	instance3 = ServicesFeaturesModel.objects.all()
	instance4 = ServicesOurServicesModel.objects.all()
	instance5 = ServicesPortfolioModel.objects.all()
	footer=Contact_Footernew.objects.all()
	copyright = CopyRightModel.objects.all()
	icon_qurey = Header_Icon_Model.objects.all()
	seo_qurey_ser = SEO_Model.objects.filter(seo_page="Services")
	seo_qurey_ser1 = SEO_Model.objects.filter(seo_page="Services")
	return render(
		request, 
		'services.html', 
		{'seo_qurey_ser1':seo_qurey_ser1, 'seo_qurey_ser':seo_qurey_ser, 'instance':instance, 'instance2':instance2, 'instance3':instance3, 
		'instance4':instance4, 'instance5':instance5, 'footer':footer, 'copyright':copyright, 'icon_qurey' : icon_qurey})

# ----------------- Services - Header ----------------------
@login_required(login_url='/login/')
def services_header_add(request):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	services_header_form = ServicesHeaderModelForm(request.POST or None, request.FILES or None )
	if services_header_form.is_valid() and request.method == "POST":
		instance = services_header_form.save(commit=False)
		instance.save()
		return redirect('services_admin_view')
	# else :
	# 	messages.error(request, "Please enter valid data......")
	return render(request, "services_header_add.html", {'copyright':copyright, 'services_header_form':services_header_form, 'current_user':current_user}) 

@login_required(login_url='/login/')
def services_header_update(request, id=None):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(ServicesHeaderModel, id=id)
	services_header_form = ServicesHeaderModelForm(request.POST or None, request.FILES or None, instance=instance)
	if request.user.is_authenticated() and services_header_form.is_valid():
		instance = services_header_form.save(commit=False)
		instance.save()
		# messages.success(request, "Updated successfully...")
		return redirect('services_admin_view')
	# else:
	# 	messages.error(request, "Failed to update...")
	return render(request, "services_header_update.html", {'copyright':copyright, 'services_header_form':services_header_form, 'current_user':current_user})

@login_required(login_url='/login/')
def services_header_delete(request, id=None):
	instance = get_object_or_404(ServicesHeaderModel, id=id)
	instance.delete()
	# messages.success(request, "Record deleted successfully...")
	return redirect('services_admin_view')

@login_required(login_url='/login/')
def services_admin_view(request):
	current_user = request.user
	context = ServicesHeaderModel.objects.all()
	context2 = ServicesGamePlanModel.objects.all()
	context3 = ServicesFeaturesModel.objects.all()
	context4 = ServicesOurServicesModel.objects.all()
	context5 = ServicesPortfolioModel.objects.all()
	copyright = CopyRightModel.objects.all()

	query = request.GET.get("q")
	if query :
		context = context.filter(header_title__icontains=query)
	if query :
		context2 = context2.filter(plan_title__icontains=query)
	if query :
		context3 = context3.filter(features_title__icontains=query)
	if query :
		context4 = context4.filter(our_title__icontains=query)
	if query :
		context5 = context5.filter(portfolio_title__icontains=query)

	return render(request, 
				'services-admin.html', 
				{'current_user':current_user,'context':context, 'context2':context2, 'context3':context3,
				'context4':context4, 'context5':context5, 'copyright':copyright,}
				)

# ----------------- Services - Header END --------------------
# ----------------- Services - Game plan ---------------------
@login_required(login_url='/login/')
def services_game_plan_add(request):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	services_plan_form = ServicesGamePlanModelForm(request.POST or None, request.FILES or None)
	if services_plan_form.is_valid() and request.method == "POST":
		instance = services_plan_form.save(commit=False)
		instance.save()
		return redirect('services_admin_view')
	# else :
	# 	messages.error(request, "Please enter valid data...")
	return render(request, "services_game_plan_add.html", 
				{'services_plan_form':services_plan_form, 'current_user':current_user, 'copyright':copyright,}
				) 

@login_required(login_url='/login/')
def services_game_plan_update(request, id=None):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(ServicesGamePlanModel, id=id)
	services_plan_form = ServicesGamePlanModelForm(request.POST or None, request.FILES or None, instance=instance )
	if  request.user.is_authenticated() and services_plan_form.is_valid():
		instance = services_plan_form.save(commit=False)
		instance.save()
		# messages.success(request, "Updated successfully...")
		return redirect('services_admin_view')
	# else:
	# 	messages.error(request, "Updation failed...")
	return render(request, "services_game_plan_update.html", {'copyright':copyright, 'services_plan_form':services_plan_form, 'current_user':current_user})

@login_required(login_url='/login/')
def services_game_plan_delete(request, id=None):
	instance = get_object_or_404(ServicesGamePlanModel, id=id)
	instance.delete()
	# messages.success(request, "Record deleted successfully...")
	return redirect('services_admin_view')


# ----------------- Services - Features ---------------------
@login_required(login_url='/login/')
def services_features_add(request):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	services_features_form = ServicesFeaturesModelForm(request.POST or None, request.FILES or None)
	if services_features_form.is_valid():
		instance = services_features_form.save(commit=False)
		instance.save()
		return redirect('services_admin_view')
	# else :
	# 	messages.error(request, "Please enter valid data...")
	return render(request, "services_features_add.html", {'copyright':copyright, 'services_features_form':services_features_form, 'current_user':current_user}) 

@login_required(login_url='/login/')
def services_features_update(request, id=None):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(ServicesFeaturesModel, id=id)
	services_features_form = ServicesFeaturesModelForm(request.POST or None, request.FILES or None, instance=instance )
	if  request.user.is_authenticated() and services_features_form.is_valid():
		instance = services_features_form.save(commit=False)
		instance.save()
		# messages.success(request, "Updated successfully...")
		return redirect('services_admin_view')
	# else:
	# 	messages.error(request, "Updation failed...")
	return render(request, "services_features_update.html", { 'copyright':copyright, 'services_features_form':services_features_form, 'current_user':current_user})

@login_required(login_url='/login/')
def services_features_delete(request, id=None):
	instance = get_object_or_404(ServicesFeaturesModel, id=id)
	instance.delete()
	# messages.success(request, "Record deleted successfully...")
	return redirect('services_admin_view')
# ----------------- Services - Features END  ----------------------

# ----------------- Services - Our Services  ----------------------
@login_required(login_url='/login/')
def services_our_services_add(request):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	services_our_form = ServicesOurServicesModelForm(request.POST or None, request.FILES or None )
	if services_our_form.is_valid() and request.method == "POST":
		instance = services_our_form.save(commit=False)
		instance.save()
		return redirect('services_admin_view')
	# if request.method == "POST":
	# instance = services_our_form.save(commit=False)
	# instance.save()
	# return redirect('services_admin_view')
	# else :
	# 	messages.error(request, "Please enter valid data...")
	return render(request, "services_our_services_add.html", {'copyright':copyright, 'services_our_form':services_our_form, 'current_user':current_user}) 

@login_required(login_url='/login/')
def services_our_services_update(request, id=None):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(ServicesOurServicesModel, id=id)
	services_our_form = ServicesOurServicesModelForm(request.POST or None, request.FILES or None, instance=instance )
	if  request.user.is_authenticated() and services_our_form.is_valid():
		instance = services_our_form.save(commit=False)
		instance.save()
		# messages.success(request, "Updated successfully...")
		return redirect('services_admin_view')
	# else:
	# 	messages.error(request, "Updation failed...")
	return render(request, "services_our_services_update.html", {'copyright':copyright, 'services_our_form':services_our_form, 'current_user':current_user})

@login_required(login_url='/login/')
def services_our_services_delete(request, id=None):
	instance = get_object_or_404(ServicesOurServicesModel, id=id)
	instance.delete()
	# messages.success(request, "Record deleted successfully...")
	return redirect('services_admin_view')
# ----------------- Services - Our Services END  ----------------------

# ----------------- Services - Portfolio  ----------------------
@login_required(login_url='/login/')
def services_portfolio_add(request):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	services_portfolio_form = ServicesPortfolioModelForm(request.POST or None, request.FILES or None )
	if services_portfolio_form.is_valid():
		instance = services_portfolio_form.save(commit=False)
		instance.save()
		return redirect('services_admin_view')
	# else :
	# 	messages.error(request, "Please enter valid data...")
	return render(request, "services_portfolio_add.html", {'copyright':copyright, 'services_portfolio_form':services_portfolio_form, 'current_user':current_user}) 

@login_required(login_url='/login/')
def services_portfolio_update(request, id=None):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(ServicesPortfolioModel, id=id)
	services_portfolio_form = ServicesPortfolioModelForm(request.POST or None, request.FILES or None, instance=instance )
	if  request.user.is_authenticated() and services_portfolio_form.is_valid():
		instance = services_portfolio_form.save(commit=False)
		instance.save()
		# messages.success(request, "Updated successfully...")
		return redirect('services_admin_view')
	# else:
	# 	messages.error(request, "Updation failed...")
	return render(request, "services_portfolio_update.html", {'copyright':copyright, 'services_portfolio_form':services_portfolio_form, 'current_user':current_user})

@login_required(login_url='/login/')
def services_portfolio_delete(request, id=None):
	instance = get_object_or_404(ServicesPortfolioModel, id=id)
	instance.delete()
	# messages.success(request, "Record deleted successfully...")
	return redirect('services_admin_view')
# ----------------- Services - Portfolio END  ----------------------


# ----------------- Contact Us  ----------------------

def contactus_view(request):
	current_user = request.user
	instance = ContactUsModel.objects.all()
	footer = Contact_Footernew.objects.all()
	copyright = CopyRightModel.objects.all()
	icon_qurey = Header_Icon_Model.objects.all()
	seo_qurey_contact = SEO_Model.objects.filter(seo_page="Contactus")
	seo_qurey_contact1 = SEO_Model.objects.filter(seo_page="Contactus")
	return render(request, "contact-us.html", {'seo_qurey_contact1':seo_qurey_contact1, 'seo_qurey_contact':seo_qurey_contact, 'icon_qurey' : icon_qurey, 'copyright':copyright,'instance':instance, 'footer':footer, 'current_user':current_user})

# ------------------- Contact-us Header ---------------------
@login_required(login_url='/login/')
def con_header_add(request):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	con_header_form = ContactUsModelForm(request.POST or None, request.FILES or None)
	if con_header_form.is_valid() and request.method == "POST" :
		instance = con_header_form.save(commit=False)
		instance.save()
		return redirect('contact_us_admin_view')
	return render(request, "contact_us_header_add.html", {'copyright':copyright, 'con_header_form':con_header_form, 'current_user':current_user})

@login_required(login_url='/login/')
def con_header_update(request, id=None):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(ContactUsModel, id=id)
	con_header_form = ContactUsModelForm(request.POST or None, request.FILES or None, instance=instance)
	if request.user.is_authenticated() and con_header_form.is_valid():
		instance = con_header_form.save(commit=False)
		instance.save()
		# messages.success(request, "Record updated successfully...")
		return redirect('contact_us_admin_view')
	return render(request, "contact_us_header_update.html", {'copyright':copyright, 'con_header_form':con_header_form, 'current_user':current_user})

@login_required(login_url='/login/')
def con_header_delete(request, id=None):
	instance = get_object_or_404(ContactUsModel, id=id)
	instance.delete()
	# messages.success(request, "Deleted successfully...")
	return redirect('contact_us_admin_view')

@login_required(login_url='/login/')
def contact_us_admin_view(request):
	current_user = request.user
	context = ContactUsModel.objects.all()
	copyright = CopyRightModel.objects.all()	
	return render(request, 'Contact_Us_admin.html', {'copyright':copyright, 'context':context, 'current_user':current_user})

def admin_panel(request):
	current_user = request.user
	context = ServicesHeaderModel.objects.all()
	context2 = ServicesGamePlanModel.objects.all()
	context3 = ServicesFeaturesModel.objects.all()
	context4 = ServicesOurServicesModel.objects.all()
	context5 = ServicesPortfolioModel.objects.all() 
	copyright = CopyRightModel.objects.all()
	icon_qurey = Header_Icon_Model.objects.all()
	query = request.GET.get("q")
	if query :
		context = context.filter(header_title__icontains=query)
	if query :
		context2 = context2.filter(plan_title__icontains=query)
	if query :
		context3 = context3.filter(features_title__icontains=query)
	if query :
		context4 = context4.filter(our_title__icontains=query)
	if query :
		context5 = context5.filter(portfolio_title__icontains=query)
	return render(request, 
				"base.html", 
				{'current_user' : current_user, 'context':context, 'context2':context2, 'context3':context3,
				'context4':context4, 'context5':context5, 'copyright':copyright, 'icon_qurey' : icon_qurey}
				)

# Public API for Services page data as JSON response
def get_services_api(request):
	queryset = ServicesHeaderModel.objects.all()
	queryset = serializers.serialize('json', queryset)
	# queryset = queryset.replace("[", "")
	# queryset = queryset.replace("]", "")
	# queryset = json.loads(queryset)

	queryset2 = ServicesGamePlanModel.objects.all()
	queryset2 = serializers.serialize('json', queryset2)
	# queryset2 = queryset2.replace("[", "")
	# queryset2 = queryset2.replace("]", "")
	# queryset2 = json.loads(queryset2)

	queryset3 = ServicesFeaturesModel.objects.all()
	queryset3 = serializers.serialize('json', queryset3)
	# queryset3 = queryset3.replace("[", "")
	# queryset3 = queryset3.replace("]", "")
	# queryset3 = json.loads(queryset3)

	queryset4 = ServicesOurServicesModel.objects.all()
	queryset4 = serializers.serialize('json', queryset4)
	# queryset4 = queryset4.replace("[", "")
	# queryset4 = queryset4.replace("]", "")
	# queryset4 = json.loads(queryset4)
	
	queryset5 = ServicesPortfolioModel.objects.all()
	queryset5 = serializers.serialize('json', queryset5)
	# queryset5 = queryset5.replace("[", "")
	# queryset5 = queryset5.replace("]", "")
	# queryset5 = json.loads(queryset5)

	to_json = {'queryset' : queryset, 'queryset2' : queryset2, 'queryset3' : queryset3, 
				'queryset4':queryset4, 'queryset5':queryset5 }
	return HttpResponse(json.dumps(to_json), content_type = 'application/json')

# Public API for Contact-us page data as JSON response
def get_contact_us_api(request):
	queryset = ContactUsModel.objects.all()
	queryset = serializers.serialize('json', queryset)
	# queryset = queryset.replace("[", "")
	# queryset = queryset.replace("]", "")
	# queryset = json.loads(queryset)

	to_json = {'queryset' : queryset, }
	return HttpResponse(json.dumps(to_json), content_type = 'application/json')
# ------------------- Portfolio - Website product -----------------------
@login_required(login_url='/login/')
def portfolio_web_product_add(request):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	portfolio_web_form = PortfolioWebsiteModelForm(request.POST or None, request.FILES or None)
	if portfolio_web_form.is_valid() and request.method == "POST" :
		instance = portfolio_web_form.save(commit=False)
		instance.save()
		return redirect('PortfolioAdmin')
	return render(request, "portfolio_web_product_add.html", {'copyright':copyright, 'portfolio_web_form':portfolio_web_form, 'current_user':current_user})

@login_required(login_url='/login/')
def portfolio_web_product_update(request, id=None):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(PortfolioWebsiteModel, id=id)
	category_list = PortfolioCategoryModel.objects.all()

	portfolio_web_form = PortfolioWebsiteModelForm(request.POST or None, request.FILES or None, instance=instance)
	if request.user.is_authenticated() and portfolio_web_form.is_valid():
		instance = portfolio_web_form.save(commit=False)
		instance.save()
		# messages.success(request, "Record updated successfully...")
		return redirect('PortfolioAdmin')
	return render(request, "portfolio_web_product_update.html", { 'copyright':copyright, 'portfolio_web_form':portfolio_web_form, 'current_user':current_user,"category_list":category_list})

@login_required(login_url='/login/')
def portfolio_web_product_delete(request, id=None):
	instance = get_object_or_404(PortfolioWebsiteModel, id=id)
	instance.delete()
	# messages.success(request, "Deleted successfully...")
	return redirect('PortfolioAdmin')
# ----------------------- Portfolio - website -products END -----------------------
# def porfolio_admin_view(request):
# 	current_user = request.user
# 	context = ServicesHeaderModel.objects.all()
# 	context2 = ServicesGamePlanModel.objects.all()
# 	context3 = ServicesFeaturesModel.objects.all()
# 	context4 = ServicesOurServicesModel.objects.all()
# 	context5 = ServicesPortfolioModel.objects.all() 

# 	query = request.GET.get("q")
# 	if query :
# 		context = context.filter(header_title__icontains=query)
# 	if query :
# 		context2 = context2.filter(plan_title__icontains=query)
# 	if query :
# 		context3 = context3.filter(features_title__icontains=query)
# 	if query :
# 		context4 = context4.filter(our_title__icontains=query)
# 	if query :
# 		context5 = context5.filter(portfolio_title__icontains=query)

# 	return render(request, 
# 				'services-admin.html', 
# 				{'current_user':current_user,'context':context, 'context2':context2, 'context3':context3,
# 				'context4':context4, 'context5':context5}
# 				)
# ---------------------------------------------------------------------------------


def portfolio_view(request):
	current_user = request.user
	instance2 = PortfolioMobileModel.objects.all()
	instance3 = PortfolioCategoryModel.objects.all()
	footer = Contact_Footernew.objects.all()
	copyright = CopyRightModel.objects.all()
	category = PortfolioCategoryModel.objects.all()
	icon_qurey = Header_Icon_Model.objects.all()
	seo_qurey_portfolio = SEO_Model.objects.filter(seo_page="Portfolio")
	seo_qurey_portfolio1 = SEO_Model.objects.filter(seo_page="Portfolio")
	
	if request.method == 'GET':
		qd = request.GET
		
		if(qd.__getitem__('category')=="ALL"):
			portfolio_data = ""
			
			portfolio_data = PortfolioWebsiteModel.objects.all()
		elif(qd.__getitem__('category')):
			portfolio_data = PortfolioWebsiteModel.objects.filter(category=qd.__getitem__('category'))
			
		else:
			portfolio_data =PortfolioWebsiteModel.objects.all()
						
	# portfolio_data = PortfolioWebsiteModel.objects.all()
		return render(request, "webpage/portfolio.html", {'seo_qurey_portfolio1':seo_qurey_portfolio1, 
			'seo_qurey_portfolio':seo_qurey_portfolio, 'category':category,'icon_qurey' : icon_qurey, 
			'copyright':copyright, 'instance':portfolio_data, 'instance2':instance2, 'instance3':instance3, 'footer':footer, 
			'current_user':current_user, 'category_selected':qd.__getitem__('category'), })


def PortfolioWebsite(request):
	current_user = request.user
	portfolio=PortfolioWebsiteModel.objects.all()
	footer=Contact_Footernew.objects.all()
	copyright = CopyRightModel.objects.all()
	icon_qurey = Header_Icon_Model.objects.all()
	return render(request, 'webpage/portfolio_website.html',{'icon_qurey' : icon_qurey, 'copyright':copyright, 'portfolio':portfolio, 'footer':footer, 'current_user':current_user})


def PortfolioMobile(request):
	current_user = request.user
	portfolio=PortfolioMobileModel.objects.all()
	footer=Contact_Footernew.objects.all()
	copyright = CopyRightModel.objects.all()
	icon_qurey = Header_Icon_Model.objects.all()
	return render(request, 'webpage/portfolio_mobile.html',{'icon_qurey' : icon_qurey, 'copyright':copyright, 'portfolio':portfolio, 'footer':footer, 'current_user':current_user})
# ------------------- Portfolio - Mobile product -----------------------

@login_required(login_url='/login/')
def portfolio_mob_product_add(request):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	portfolio_mob_form = PortfolioMobileModelForm(request.POST or None, request.FILES or None)
	if portfolio_mob_form.is_valid() and request.method == "POST" :
		instance = portfolio_mob_form.save(commit=False)
		instance.save()
		return redirect('PortfolioAdmin')
	return render(request, "portfolio_mob_product_add.html", { 'copyright':copyright, 'portfolio_mob_form':portfolio_mob_form, 'current_user':current_user})

@login_required(login_url='/login/')
def portfolio_mob_product_update(request, id=None):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(PortfolioMobileModel, id=id)
	portfolio_mob_form = PortfolioMobileModelForm(request.POST or None, request.FILES or None, instance=instance)
	if request.user.is_authenticated() and portfolio_mob_form.is_valid():
		instance = portfolio_mob_form.save(commit=False)
		instance.save()
		# messages.success(request, "Record updated successfully...")
		return redirect('PortfolioAdmin')
	return render(request, "portfolio_mob_product_update.html", {'copyright':copyright, 'portfolio_mob_form':portfolio_mob_form, 'current_user':current_user})

@login_required(login_url='/login/')
def portfolio_mob_product_delete(request, id=None):
	instance = get_object_or_404(PortfolioMobileModel, id=id)
	instance.delete()
	# messages.success(request, "Deleted successfully...")
	return redirect('PortfolioAdmin')
# ----------------------- Portfolio - Mobile -products END -----------------------


# ------------------- Portfolio -Category -----------------------

@login_required(login_url='/login/')
def portfolio_category_add(request):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	portfolio_cat_form = PortfolioCategoryModelForm(request.POST or None)
	if portfolio_cat_form.is_valid() and request.method == "POST" :
		instance = portfolio_cat_form.save(commit=False)
		instance.save()
		return redirect('PortfolioAdmin')
	return render(request, "portfolio_category_add.html", { 'copyright':copyright, 'portfolio_cat_form':portfolio_cat_form, 'current_user':current_user})

@login_required(login_url='/login/')
def portfolio_category_update(request, id=None):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(PortfolioCategoryModel, id=id)
	portfolio_cat_form = PortfolioCategoryModelForm(request.POST or None, request.FILES or None, instance=instance)
	if request.user.is_authenticated() and portfolio_cat_form.is_valid():
		instance = portfolio_cat_form.save(commit=False)
		instance.save()
		# messages.success(request, "Record updated successfully...")
		return redirect('PortfolioAdmin')
	return render(request, "portfolio_category_update.html", {'copyright':copyright, 'portfolio_cat_form':portfolio_cat_form, 'current_user':current_user})

@login_required(login_url='/login/')
def portfolio_category_delete(request, id=None):
	instance = get_object_or_404(PortfolioCategoryModel, id=id)
	instance.delete()
	# messages.success(request, "Deleted successfully...")
	return redirect('PortfolioAdmin')
# ----------------------- Portfolio - category END -----------------------

# --------------------- Article ------------------------------------------

def article_view(request):
	current_user = request.user
	instance = ArticleModel.objects.all()
	footer = Contact_Footernew.objects.all()
	copyright = CopyRightModel.objects.all()
	icon_qurey = Header_Icon_Model.objects.all()	
	return render(request, "articles.html", {'icon_qurey' : icon_qurey,'copyright':copyright, 'instance':instance, 'footer':footer, 'current_user':current_user})

@login_required(login_url='/login/')
def article_admin(request):
	current_user = request.user
	instance = ArticleModel.objects.all()
	copyright = CopyRightModel.objects.all()
	return render(request, "article_admin.html", {'copyright':copyright, 'instance':instance, 'current_user':current_user,})

@login_required(login_url='/login/')
def article_add(request):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	article_form = ArticleModelForm(request.POST or None, request.FILES or None)
	if article_form.is_valid() and request.method == "POST" :
		instance = article_form.save(commit=False)
		instance.save()
		return redirect('article_admin')
	return render(request, 'articles_add.html', {'copyright':copyright, 'article_form':article_form, 'current_user' : current_user})

@login_required(login_url='/login/')
def article_update(request, id=None):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(ArticleModel, id=id)
	article_form = ArticleModelForm(request.POST or None, request.FILES or None, instance=instance)
	if article_form.is_valid() and request.method == "POST" :
		instance = article_form.save(commit=False)
		instance.save()
		return redirect('article_admin')
	return render(request, 'articles_update.html', {'copyright':copyright, 'article_form':article_form})

@login_required(login_url='/login/')
def article_delete(request, id=None):
	current_user = request.user
	instance = get_object_or_404(ArticleModel, id=id)
	instance.delete()
	return redirect('article_admin')

# ------------------- Portfolio -Category -----------------------

@login_required(login_url='/login/')
def copy_right_admin(request):
	current_user = request.user
	instance = CopyRightModel.objects.all()
	copyright = CopyRightModel.objects.all()
	return render(request, "copy_right_admin.html", {'copyright':copyright, 'instance':instance, 'current_user':current_user,})

@login_required(login_url='/login/')
def copy_right_quote_add(request):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	copy_right_form = CopyRightModelForm(request.POST or None)
	if copy_right_form.is_valid() and request.method == "POST" :
		instance = copy_right_form.save(commit=False)
		instance.save()
		return redirect('copy_right_admin')
	return render(request, "copy_right_add.html", { 'copyright':copyright, 'copy_right_form':copy_right_form, 'current_user':current_user})

@login_required(login_url='/login/')
def copy_right_quote_update(request, id=None):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(CopyRightModel, id=id)
	copy_right_form = CopyRightModelForm(request.POST or None, request.FILES or None, instance=instance)
	if request.user.is_authenticated() and copy_right_form.is_valid():
		instance = copy_right_form.save(commit=False)
		instance.save()
		# messages.success(request, "Record updated successfully...")
		return redirect('copy_right_admin')
	return render(request, "copy_right_update.html", {'copyright':copyright, 'copy_right_form':copy_right_form, 'current_user':current_user})

@login_required(login_url='/login/')
def copy_right_quote_delete(request, id=None):
	instance = get_object_or_404(CopyRightModel, id=id)
	instance.delete()
	# messages.success(request, "Deleted successfully...")
	return redirect('copy_right_admin')
# ----------------------- Portfolio - category END -----------------------

#---------------prashant Views------------------------

def Footer(request):
	footer=Contact_Footernew.objects.all()
	copyright = CopyRightModel.objects.all()
	icon_qurey = Header_Icon_Model.objects.all()
	return render(request, 'webpage/footer.html',{'icon_qurey' : icon_qurey, 'footer':footer, 'copyright':copyright})

def About_us(request):
	header=About_us_HeaderImg.objects.all()
	body=About_us_Body.objects.get(id="1")
	body2=About_us_Body.objects.get(id="2")
	bodyimg=About_us_BodyImg.objects.all()
	img=About_us_Img.objects.all()

	footer=Contact_Footernew.objects.all()
	copyright = CopyRightModel.objects.all()
	icon_qurey = Header_Icon_Model.objects.all()
	seo_qurey_about = SEO_Model.objects.filter(seo_page="Aboutus")
	seo_qurey_about1 = SEO_Model.objects.filter(seo_page="Aboutus")
	return render(request, 'webpage/aboutus.html',{'seo_qurey_about1':seo_qurey_about1, 'seo_qurey_about':seo_qurey_about, 'icon_qurey' : icon_qurey, 'header':header,'body':body,'body2':body2,'bodyimg':bodyimg,'img':img,'footer':footer, 'copyright':copyright})


def Portfolio(request):
	portfolio=Portfolio_Page.objects.all()
	footer=Contact_Footernew.objects.all()
	copyright = CopyRightModel.objects.all()
	icon_qurey = Header_Icon_Model.objects.all()
	return render(request, 'webpage/portfolio.html',{'icon_qurey' : icon_qurey, 'copyright':copyright, 'portfolio':portfolio,'footer':footer})

#----------Admin Panel View------------------------------------------------
@login_required(login_url='/login/')
def AbpoutAdmin(request):
	current_user = request.user
	header=About_us_HeaderImg.objects.all()
	body=About_us_Body.objects.all()
	
	bodyimg=About_us_BodyImg.objects.all()
	img=About_us_Img.objects.all()

	copyright = CopyRightModel.objects.all()
	return render(request, 'webpage/Aboutus_Admin.html',{'copyright':copyright,'header':header,'body':body,'bodyimg':bodyimg,'img':img, 'current_user':current_user})

@login_required(login_url='/login/')
def About_us_HeaderImg_update(request,id=None):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(About_us_HeaderImg,id=id)
	form=About_us_HeaderImg_Form(request.POST or None,request.FILES or None,instance=instance)
	if form.is_valid():
		instance=form.save(commit=False)
		instance.save()
		return redirect('AbpoutAdmin')
	
	return render(request, 'webpage/AboutHeaderUpdate.html',{ 'copyright':copyright, 'form':form, 'current_user':current_user})

@login_required(login_url='/login/')
def About_us_Body_update(request,id=None):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(About_us_Body,id=id)
	form=About_us_Body_Form(request.POST or None,request.FILES or None,instance=instance)
	if form.is_valid():
		instance=form.save(commit=False)
		instance.save()
		return redirect('AbpoutAdmin')
	
	return render(request, 'webpage/AboutusBodyUpdate.html',{ 'copyright':copyright, 'form':form, 'current_user':current_user})

@login_required(login_url='/login/')
def About_us_BodyImg_update(request,id=None):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(About_us_BodyImg,id=id)
	form=About_us_BodyImg_Form(request.POST or None,request.FILES or None,instance=instance)
	if form.is_valid():
		instance=form.save(commit=False)
		instance.save()
		return redirect('AbpoutAdmin')
	
	return render(request, 'webpage/AboutusBodyImgupdate.html',{'copyright':copyright, 'form':form, 'current_user':current_user})

@login_required(login_url='/login/')
def About_us_Img_update(request,id=None):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(About_us_Img,id=id)
	form=About_us_Img_Form(request.POST or None,request.FILES or None,instance=instance)
	if form.is_valid():
		instance=form.save(commit=False)
		instance.save()
		return redirect('AbpoutAdmin')
	
	return render(request, 'webpage/AboutusImgupdate.html',{'copyright':copyright, 'form':form, 'current_user':current_user})


@login_required(login_url='/login/')
def About_us_Img_Delete(request,id=None):
	instance = get_object_or_404(About_us_Img, id=id)
	instance.delete()
	return redirect('AbpoutAdmin')


@login_required(login_url='/login/')
def About_us_Img_Add(request,id=None):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	form=About_us_Img_Form(request.POST or None,request.FILES or None)
	if form.is_valid():
		instance=form.save(commit=False)
		instance.save()
		return redirect('AbpoutAdmin')
	
	return render(request, 'webpage/AboutusImgAdd.html',{'copyright':copyright, 'form':form, 'current_user':current_user})


@login_required(login_url='/login/')
def PortfolioAdmin(request):
	current_user = request.user
	instance=PortfolioWebsiteModel.objects.all()
	instance2=PortfolioMobileModel.objects.all()
	instance3 = PortfolioCategoryModel.objects.all()
	copyright = CopyRightModel.objects.all()
	
	return render(request, 'webpage/PortfolioAdmin.html',{'instance3':instance3, 'copyright':copyright, 'instance':instance, 'instance2':instance2, 'current_user':current_user})



@login_required(login_url='/login/')
def PortfolioDelete(request,id=None):
	instance = get_object_or_404(Portfolio_Page, id=id)
	instance.delete()
	return redirect('PortfolioAdmin')

@login_required(login_url='/login/')
def ContactAdmin(request):
	current_user = request.user
	footer=Contact_Footernew.objects.all()
	copyright = CopyRightModel.objects.all()
	return render(request, 'webpage/ContactAdmin.html',{'copyright':copyright,  'footer':footer, 'current_user':current_user})

@login_required(login_url='/login/')
def ContactUpdate(request,id=None):
	current_user = request.user
	instance = get_object_or_404(Contact_Footernew,id=id)
	copyright = CopyRightModel.objects.all()
	form=Contact_Footer_Form(request.POST or None,request.FILES or None,instance=instance)
	if form.is_valid():
		instance=form.save(commit=False)
		instance.save()
		return redirect('ContactAdmin')
	return render(request, 'webpage/Contactupdate.html',{'copyright':copyright,  'form':form, 'current_user':current_user})

@login_required(login_url='/login/')
def ContactDelete(request,id=None):
	instance = get_object_or_404(Contact_Footernew,id=id)
	instance.delete()
	return redirect('ContactAdmin')

@login_required(login_url='/login/')
def ContactAdd(request,id=None):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	form=Contact_Footer_Form(request.POST or None,request.FILES or None)
	if form.is_valid():
		instance=form.save(commit=False)
		instance.save()
		return redirect('ContactAdmin')
	
	return render(request, 'webpage/ContactAdd.html',{'copyright':copyright, 'form':form, 'current_user':current_user})




#-----------------json calling-----------------------
def get_About_us(request):
	queryset1=About_us_HeaderImg.objects.all()
	queryset1=serializers.serialize('json',queryset1)
	
	queryset1=queryset1.replace("[", "")
	queryset1=queryset1.replace("]", "")
	queryset1=json.loads(queryset1)
	
	queryset2=About_us_Body.objects.all()
	queryset2=serializers.serialize('json',queryset2)
	
	queryset2=json.loads(queryset2)
	

	queryset3=About_us_BodyImg.objects.all()
	queryset3=serializers.serialize('json',queryset3)


	queryset4=About_us_Img.objects.all()
	queryset4=serializers.serialize('json',queryset4)
	
	json2={"queryset1":queryset1,"queryset2":queryset2,"queryset3":queryset3,"queryset4":queryset4}
	return HttpResponse(json.dumps(json2), content_type='application/json')

def get_portfolio(request):
	queryset1=Portfolio_Page.objects.all()
	queryset1=serializers.serialize('json',queryset1)
	
	
	json2={"queryset1":queryset1}
	return HttpResponse(json.dumps(json2), content_type='application/json')


# ------------- Naresh ----------------------

def hms_admin_page(request): # this view use for admin site add update and delete
	current_user = request.user
	queryset1= Top_image_icon.objects.all()
	queryset2= Middle_image_icon.objects.all()
	queryset3= Buttom_image_icon.objects.all()
	queryset4= Text_button.objects.all()
	footer=Contact_Footernew.objects.all()
	copyright = CopyRightModel.objects.all()
	icon_qurey = Header_Icon_Model.objects.all()
	seo_qurey_hms = SEO_Model.objects.filter(seo_page="HMS")
	seo_qurey_hms1 = SEO_Model.objects.filter(seo_page="HMS")

	return render(request, 'webpage/admin_site.html', {'seo_qurey_hms1':seo_qurey_hms1, 'seo_qurey_hms':seo_qurey_hms, 'icon_qurey' : icon_qurey, 'queryset1':queryset1, 'queryset2':queryset2, 'queryset3':queryset3, 'queryset4':queryset4,'footer':footer, 'copyright':copyright,})

@login_required(login_url='/login/')
def hms_updatation_page(request): # this view use for the updation of 
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(Top_image_icon)
	form = Comment_Form1(request.POST or None, request.FILES or None, instance=instance)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		# messages.success(request, "Update successfully..")
		return redirect('/hms-admin')
		# return HttpResponseRedirect(instance.get_absolute_url())
	# else:
	# 	messages.error(request, "Reupdation failed..")
	return render(request, "webpage/update.html", {'copyright':copyright, 'form':form, 'current_user':current_user})

@login_required(login_url='/login/')
def hms_middle_updatation(request): # this view use for the 
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(Middle_image_icon)
	form = Comment_Form2(request.POST or None, request.FILES or None, instance=instance)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		# messages.success(request, "Update successfully..")
		return redirect('/hms-admin')
		# return HttpResponseRedirect(instance.get_absolute_url())
	# else:
	# 	messages.error(request, "Reupdation failed..")
	return render(request, "webpage/middle_hms_update.html", {'copyright':copyright, 'form':form, 'current_user':current_user})


@login_required(login_url='/login/')
def hms_buttom_updatation(request, id=None): # this view use for the updation of 
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(Buttom_image_icon, id=id)
	form = Comment_Form3(request.POST or None, request.FILES or None, instance=instance)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		# messages.success(request, "Update successfully..")
		return redirect('/hms-admin')
		
	# else:
	# 	messages.error(request, "Reupdation failed..")
	return render(request, "webpage/buttom_hms_update.html", {'copyright':copyright, 'form':form, 'current_user':current_user})


@login_required(login_url='/login/')
def form_update(request): # this view use for the updation of 
 	current_user = request.user
 	queryset1 = Top_image_icon.objects.all()
 	queryset2 = Middle_image_icon.objects.all()
 	queryset3 = Buttom_image_icon.objects.all()
 	copyright = CopyRightModel.objects.all()
 	#queryset2 = Contact_Address_txt.objects.all()
 	query = request.GET.get("q")
 	return render(request, 'webpage/hms_update.html', {'copyright':copyright, 'queryset1':queryset1, 'queryset2':queryset2, 'queryset3':queryset3, 'current_user':current_user})

@login_required(login_url='/login/')
def buttom_delete(request, id=None): # this view use for delete 
	instance = get_object_or_404(Buttom_image_icon, id=id)
	instance.delete()
	# messages.success(request, "Record deleted successflly..")
	return redirect('/hms-admin')
	# return render(request, "webpage/hms_update.html", {})

@login_required(login_url='/login/')
def hms_add_new(request): # this view use for add new
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	if request.method == 'POST':
		image_icon = request.FILES['image_icon']
		icon2 = request.FILES['icon2']
		title2 = request.POST['title2']
		image_url = request.POST['image_url']
		instance = Buttom_image_icon(image_icon=image_icon, icon2=icon2, title2=title2, image_url=image_url)
		instance.save()
		return redirect('/hms-admin')
	return render(request, 'webpage/hms_add_new.html', {'copyright':copyright, 'current_user':current_user})


#Here Blog page code 

def blog_admin_page(request):
	queryset5 = Blog_page.objects.all()
	footer=Contact_Footernew.objects.all()
	copyright = CopyRightModel.objects.all()
	icon_qurey = Header_Icon_Model.objects.all()
	seo_qurey_blog = SEO_Model.objects.filter(seo_page="Blog")
	seo_qurey_blog1 = SEO_Model.objects.filter(seo_page="Blog")
	return render(request, 'webpage/blog_admin_site.html', {'seo_qurey_blog1':seo_qurey_blog1, 'seo_qurey_blog':seo_qurey_blog, 'icon_qurey' : icon_qurey, 'queryset5':queryset5,'footer':footer, 'copyright':copyright,})

@login_required(login_url='/login/')
def blog_from_update(request):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	queryset5 = Blog_page.objects.all()
	query = request.GET.get("q")
	return render(request, 'webpage/blog_page_update.html', {'copyright':copyright, 'queryset5':queryset5, 'current_user':current_user})

@login_required(login_url='/login/')
def blog_page_updation(request, id=None):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(Blog_page, id=id)
	form = Blog_Form(request.POST or None, request.FILES or None, instance=instance)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		# messages.success(request, "Update successfully..")
		return redirect('/blog-admin')
	# else:
	# 	messages.error(request, "Reupdation failed..")
	return render(request, "webpage/blog_update.html", {'copyright':copyright, 'form':form, 'current_user':current_user})

@login_required(login_url='/login/')
def bolg_delete(request, id=None): # this view use for delete 
	instance = get_object_or_404(Blog_page, id=id)
	instance.delete()
	# messages.success(request, "Record deleted successflly..")
	return redirect('/blog-admin')
	# return render(request, "webpage/blog_page_update.html", {})

@login_required(login_url='/login/')
def blog_add_new(request): # this view use for add new
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	if request.method == 'POST':
		blog_image = request.FILES['blog_image']
		date_no = request.POST['date_no']
		date_char = request.POST['date_char']
		#calender1 = request.POST['calender1']
		calender = request.POST['calender']
		date_char = request.POST['date_char']
		by = request.POST['by']
		bolg_title = request.POST['bolg_title']
		bolg_discription = request.POST['bolg_discription']		
		instance = Blog_page(blog_image=blog_image, date_no=date_no, date_char=date_char, calender=calender, by=by, bolg_title=bolg_title, bolg_discription=bolg_discription)
		instance.save()
		return redirect('/blog-admin')
	return render(request, 'webpage/blog_add_new.html', {'copyright':copyright, 'current_user':current_user})


#here Home Index page Coding
def home_index_page(request):
	current_user = request.user
	queryset6 = Home_index_page1.objects.all()
	queryset7 = Home_index_page2.objects.all()
	queryset71 = Home_middle_page1.objects.all()
	queryset72 = Home_middle_page2.objects.all()
	queryset73 = Home_middle_page3.objects.all()
	queryset8 = Home_index_page3.objects.all()
	queryset9 = Home_index_page4.objects.all()
	footer=Contact_Footernew.objects.all()
	copyright = CopyRightModel.objects.all()
	icon_qurey = Header_Icon_Model.objects.all()
	seo_qurey_home = SEO_Model.objects.filter(seo_page="Home")
	seo_qurey_home1 = SEO_Model.objects.filter(seo_page="Home")

	return render(request, 'webpage/index_admin_panel.html', {'seo_qurey_home1':seo_qurey_home1, 'seo_qurey_home':seo_qurey_home, 'icon_qurey' : icon_qurey, 'current_user':current_user, 'queryset6':queryset6, 'queryset7':queryset7, 'queryset8':queryset8, 'queryset9':queryset9, 'queryset71':queryset71, 'queryset72':queryset72, 'queryset73':queryset73,'footer':footer, 'copyright':copyright,})

@login_required(login_url='/login/')
def main_update_page(request): # this view use for the updation of 
 	current_user = request.user
 	queryset6 = Home_index_page1.objects.all()
 	queryset7 = Home_index_page2.objects.all()
 	queryset71 = Home_middle_page1.objects.all()
 	queryset72 = Home_middle_page2.objects.all()
 	queryset73 = Home_middle_page3.objects.all()
 	queryset8 = Home_index_page3.objects.all()
 	queryset9 = Home_index_page4.objects.all()
 	copyright = CopyRightModel.objects.all()

 	query = request.GET.get("q")
 	return render(request, 'webpage/index_main_update_page.html', {'copyright':copyright,'current_user':current_user, 'queryset6':queryset6, 'queryset7':queryset7, 'queryset8':queryset8, 'queryset9':queryset9, 'queryset71':queryset71, 'queryset72':queryset72, 'queryset73':queryset73})

@login_required(login_url='/login/')
def home_page__top_update(request):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(Home_index_page1)
	form = Index_Form1(request.POST or None, request.FILES or None, instance=instance)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		# messages.success(request, "Update successfully..")
		return redirect('/home-admin')
	# else:
	# 	messages.error(request, "Reupdation failed..")
	return render(request, "webpage/home_update.html", {'copyright':copyright, 'form':form, 'current_user':current_user})

@login_required(login_url='/login/')
def home_middle_update(request):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(Home_index_page2)
	form = Index_Form2(request.POST or None, request.FILES or None, instance=instance)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		# messages.success(request, "Update successfully..")
		return redirect('/home-admin')
	# else:
	# 	messages.error(request, "Reupdation failed..")
	return render(request, "webpage/home_mi1_update.html", {'copyright':copyright, 'form':form, 'current_user':current_user})

@login_required(login_url='/login/')
def home_add_new(request): # this view use for add new
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	if request.method == 'POST':
		image_bg1 = request.FILES['image_bg1']
		title_index1 = request.POST['title_index1']
		sub_title = request.POST['sub_title']
		instance = Home_index_page1(image_bg1=image_bg1, title_index1=title_index1, sub_title=sub_title)
		instance.save()
		return redirect('/home-admin')
	return render(request, 'webpage/home_add_new.html', { 'copyright':copyright, 'current_user':current_user})

@login_required(login_url='/login/')
def home_middle2_update(request, id=None):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(Home_middle_page1, id=id)
	form = Index_Form_middle(request.POST or None, request.FILES or None, instance=instance)
	if form.is_valid():
	    instance = form.save(commit=False)
	    instance.save()
	    # messages.success(request, "Update successfully..")
	    return redirect('/home-admin')
	# else:
	#     messages.error(request, "Reupdation failed..")
	return render(request, "webpage/home_mi2_update.html", {'copyright':copyright, 'form':form, 'current_user':current_user})

@login_required(login_url='/login/')
def home_middle3_update(request):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(Home_middle_page2)
	form = Index_Form_middle1(request.POST or None, request.FILES or None, instance=instance)
	if form.is_valid():
	    instance = form.save(commit=False)
	    instance.save()
	    # messages.success(request, "Update successfully..")
	    return redirect('/home-admin')
	# else:
	#     messages.error(request, "Reupdation failed..")
	return render(request, "webpage/home_mi3_update.html", {'copyright':copyright, 'form':form, 'current_user':current_user})

@login_required(login_url='/login/')
def home_middle4_update(request):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(Home_middle_page3)
	form = Index_Form_middle2(request.POST or None, request.FILES or None, instance=instance)
	if form.is_valid():
	    instance = form.save(commit=False)
	    instance.save()
	    # messages.success(request, "Update successfully..")
	    return redirect('/home-admin')
	# else:
	#     messages.error(request, "Reupdation failed..")
	return render(request, "webpage/home_mi4_update.html", {'copyright':copyright, 'form':form, 'current_user':current_user})


@login_required(login_url='/login/')
def home_semi_middle_update(request):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(Home_index_page3)
	form = Index_Form3(request.POST or None, request.FILES or None, instance=instance)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		# messages.success(request, "Update successfully..")
		return redirect('/home-admin')
	# else:
	# 	messages.error(request, "Reupdation failed..")
	return render(request, "webpage/home_semi_mi_update.html", {'copyright':copyright,  'form':form, 'current_user':current_user})

@login_required(login_url='/login/')
def home_buttom_update(request):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(Home_index_page4)
	form = Index_Form4(request.POST or None, request.FILES or None, instance=instance)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		# messages.success(request, "Update successfully..")
		return redirect('/home-admin')
	# else:
	# 	messages.error(request, "Reupdation failed..")
	return render(request, "webpage/home_buttom_update.html", {'copyright':copyright,  'form':form, 'current_user':current_user})

# JSON CODE
def home_hms_blog_code(request): # this view use for show contact Us page code on json
 	queryset1 = Top_image_icon.objects.all()
 	queryset1 = serializers.serialize('json', queryset1)
 	
 	queryset2 = Middle_image_icon.objects.all()
 	queryset2 = serializers.serialize('json', queryset2)
 	
 	queryset3 = Buttom_image_icon.objects.all()
 	queryset3 = serializers.serialize('json', queryset3)
 	
 	queryset4 = Blog_page.objects.all()
 	queryset4 = serializers.serialize('json', queryset4)
 	
 	queryset5 = Home_index_page1.objects.all()
 	queryset5 = serializers.serialize('json', queryset5)
 	
 	queryset6 = Home_index_page2.objects.all()
 	queryset6 = serializers.serialize('json', queryset6)
 
 	queryset61 = Home_middle_page1.objects.all()
 	queryset61 = serializers.serialize('json', queryset61)

 	queryset62 = Home_middle_page2.objects.all()
 	queryset62 = serializers.serialize('json', queryset62)

 	queryset63 = Home_middle_page3.objects.all()
 	queryset63 = serializers.serialize('json', queryset63)
 
 	queryset7 = Home_index_page3.objects.all()
 	queryset7 = serializers.serialize('json', queryset7)
 	
 	queryset8 = Home_index_page4.objects.all()
 	queryset8 = serializers.serialize('json', queryset8)


 	to_json = {'queryset1':queryset1, 'queryset2':queryset2, 'queryset3':queryset3, 'queryset4':queryset4, 'queryset5':queryset5, 'queryset6':queryset6, 'queryset7':queryset7, 'queryset8':queryset8, 'queryset61':queryset61, 'queryset62':queryset62, 'queryset63':queryset63}
 	return HttpResponse(json.dumps(to_json), content_type="application/json")

# ------------Naresh email code--------------------
def email_send(request):
	footer=Contact_Footernew.objects.all()
	copyright = CopyRightModel.objects.all()
	icon_qurey = Header_Icon_Model.objects.all()
	if request.method == 'POST':
		instance = ContactUsModel.objects.all()
		name= request.POST['name']
		email = request.POST['email']
		subject = request.POST['subject']
		message = request.POST['message']
		#return redirect('contact-us.html')
		fullname =   "NAME:" + " " + name
		fullemail = "\n " "NAME:" + " " + name + "\n " "EMAIL:" + " " + email + "\n " "RELATED TO:" + " " + subject + " \n " "DESCRIPTION:" + " " + message + ""  
		send_mail(fullname,fullemail,"no-reply@amcits.sg",['enquiries@amcits.sg'])
	return render(request, 'email_sent_page.html', {'footer':footer, 'copyright':copyright, 'icon_qurey':icon_qurey})

def email_sent_page(request):
	if request.method == 'POST':
		instance = ContactUsModel.objects.all()
		name = request.POST['name']
		email = request.POST['email']
		subject = request.POST['subject']
		message = request.POST['message']
	return render(request, 'email_sent_page.html', {})

# ------------- Dynamic Footer ------------------
 
def base_view(request):
	copyright = CopyRightModel.objects.all()
	icon_qurey = Header_Icon_Model.objects.all()

	return render(request, 'base.html', {'icon_qurey' : icon_qurey, 'copyright':copyright,})

# ------------ Social Media Icons ----------------

#--------front end ----------------
def header_icon_view(request):
	footer=Contact_Footernew.objects.all()
	icon_qurey = Header_Icon_Model.objects.all()
	return render(request, 'webpage/header.html', {'footer':footer, 'icon_qurey':icon_qurey})


#update code Header
@login_required(login_url='/login/')
def header_icon_admin(request): # this view use for the updation of 
 	icon_qurey = Header_Icon_Model.objects.all()
 	query = request.GET.get("q")
 	current_user = request.user
 	copyright = CopyRightModel.objects.all()
 	return render(request, 'webpage/header_icon_update_page.html', {'current_user':current_user, 'copyright':copyright, 'icon_qurey':icon_qurey})

@login_required(login_url='/login/')
def header_icon_add(request): # this view use for add new
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	if request.method == 'POST':
		icon_image = request.FILES['icon_image']
		icon_title = request.POST['icon_title']
		icon_url = request.POST['icon_url']	
		instance = Header_Icon_Model(icon_image=icon_image, icon_title=icon_title, icon_url=icon_url)
		instance.save()
		return redirect('header_icon_admin')
	return render(request, 'webpage/icon_header_add_new.html', {'current_user':current_user, 'copyright':copyright,})

@login_required(login_url='/login/')
def header_icon_update(request, id=None):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(Header_Icon_Model, id=id)
	form = Header_Icon(request.POST or None, request.FILES or None, instance=instance)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		messages.success(request, "Update successfully..")
		return redirect('header_icon_admin')
		#return HttpResponseRedirect(instance.get_absolute_url())
	return render(request, "webpage/header_icon_form_update.html", {'current_user':current_user, 'copyright':copyright,'form':form})

@login_required(login_url='/login/')
def header_icon_delete(request, id=None): # this view use for delete 
	instance = get_object_or_404(Header_Icon_Model, id=id)
	instance.delete()
	# messages.success(request, "Record deleted successflly..")
	return redirect('header_icon_admin')
	# return render(request, "webpage/blog_page_update.html", {})

#------------SEO Views------------
def SEO_view(request):
	footer=Contact_Footernew.objects.all()
	seo_qurey = SEO_Model.objects.filter(seo_title="HMS")
	

	return render(request, 'webpage/seo.html', {'footer':footer, 'seo_qurey':"seo_qurey"})

@login_required(login_url='/login/')
def seo_admin(request):
	
	seo_qurey = SEO_Model.objects.all()
	query = request.GET.get("q")
	return render(request, 'webpage/seo-admin.html', {'seo_qurey':seo_qurey})

@login_required(login_url='/login/')
def seo_update_hms(request, id=None): 
	instance = get_object_or_404(SEO_Model, id=id)
	form = Seo_form(request.POST or None, request.FILES or None, instance=instance)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		messages.success(request, "Update successfully..")
		return redirect('seo_admin')
		
	return render(request, "webpage/seo_update.html", {'form':form})

@login_required(login_url='/login/')
def seo_add(request): # this view use for add new
	
	
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	'''form = Seo_form(request.POST or None)
	#form = Seo_form(request.POST)
	if request.method == 'POST':
		instance = form.save(commit=False)

		instance.save()
		return redirect('seo_admin')
	
	return render(request, 'webpage/seo_add.html', {'current_user':current_user, 'form':form, 'copyright':copyright})
'''
	form = Seo_form(request.POST or None)
	if form.is_valid():
		instance = form.save(commit=False)

		instance.save()
		return redirect('seo_admin')
	
	return render(request, 'webpage/seo_add.html', {'current_user':current_user, 'form':form, 'copyright':copyright})
def seo_delete(request, id=None): 
	instance = get_object_or_404(SEO_Model, id=id)
	instance.delete()
	return redirect('seo_admin')

# ------------- Blog page --------------------

# def blog_post_view(request):
# 	post = Blog.objects.all()
# 	footer=Contact_Footernew.objects.all()
# 	copyright = CopyRightModel.objects.all()
# 	icon_qurey = Header_Icon_Model.objects.all()
# 	seo_qurey_ser = SEO_Model.objects.filter(seo_page="Blog")
# 	seo_qurey_ser1 = SEO_Model.objects.filter(seo_page="Blog")

# 	return render(
# 		request, 
# 		'services.html', 
# 		{'seo_qurey_ser1':seo_qurey_ser1, 'seo_qurey_ser':seo_qurey_ser, 'instance':instance, 'instance2':instance2, 'instance3':instance3, 
# 		'instance4':instance4, 'instance5':instance5, 'footer':footer, 'copyright':copyright, 'icon_qurey' : icon_qurey})

# ----------------- Services - Header ----------------------
# @login_required(login_url='/login/')
# def blog_post_admin(request):





# @login_required(login_url='/login/')
# def blog_post_add(request):
# 	if request.method == "POST":
# 		form = PostForm(request.POST)
# 		if form.is_valid():
# 			post = form.save(commit=False)
# 			post.author	= request.user
# 			post.published_date	= timezone.now()
# 			post.save()
# 			return redirect('post_detail', pk=post.pk)
# 	else:
# 		form = PostForm()
# 	return render(request, 'blog/post_edit.html', {'form': form})


# @login_required(login_url='/login/')
# def blog_post_update(request, id=None):
# 	post = get_object_or_404(Post, pk=pk)
# 	if request.method == "POST":
# 		form = PostForm(request.POST, instance=post)
# 		if form.is_valid():
# 			post = form.save(commit=False)
# 			post.author	= request.user
# 			post.published_date	= timezone.now()
# 			post.save()
# 			return redirect('post_detail', pk=post.pk)
# 	else:
# 		form = PostForm(instance=post)
# 	return render(request, 'blog/post_edit.html', {'form': form})


# @login_required(login_url='/login/')
# def blog_post_delete(request, id=None):
# 	post = get_object_or_404(Blog, id=id)
# 	post.delete()
# 	return redirect('blog_post_admin_view')

# -------------------- Blog category ------------------

@login_required(login_url='/login/')
def blog_category_add(request):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	blog_form = BlogCategoryModelForm(request.POST or None)
	if blog_form.is_valid() and request.method == "POST" :
		instance = blog_form.save(commit=False)
		instance.save()
		return redirect('blog_post_admin')
	return render(request, "blog_category_add.html", { 'copyright':copyright, 'blog_form':blog_form, 'current_user':current_user})

@login_required(login_url='/login/')
def blog_category_update(request, id=None):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(BlogCategoryModel, id=id)
	blog_form = BlogCategoryModelForm(request.POST or None, request.FILES or None, instance=instance)
	if request.user.is_authenticated() and blog_form.is_valid():
		instance = blog_form.save(commit=False)
		instance.save()
		return redirect('blog_post_admin')
	return render(request, "blog_category_upload.html", {'copyright':copyright, 'blog_form':blog_form, 'current_user':current_user})

@login_required(login_url='/login/')
def blog_category_delete(request, id=None):
	instance = get_object_or_404(BlogCategoryModel, id=id)
	instance.delete()
	return redirect('blog_post_admin')





# ------------------ Blog page ------------------------

def blog_post_view(request):
	current_user = request.user
	# first_post = BlogModel.objects.all().first()
	# first_post = BlogModel.objects.all()[:1].get()
	# post = BlogModel.objects.all()
	# post_all = BlogModel.objects.exclude(id=first_post.id)
	post_all = BlogModel.objects.all().order_by('created_date')
	category = BlogCategoryModel.objects.all()
	footer=Contact_Footernew.objects.all()
	copyright = CopyRightModel.objects.all()
	icon_qurey = Header_Icon_Model.objects.all()
	seo_qurey_blog = SEO_Model.objects.filter(seo_page="Blog")
	seo_qurey_blog1 = SEO_Model.objects.filter(seo_page="Blog")

	if request.method == 'GET':
		qd = request.GET
		if(qd.__getitem__('category')=="ALL"):
			post_all = ""
			# first_post = BlogModel.objects.all()[:1].get()
			# post_all = BlogModel.objects.exclude(id=first_post.id)
			post_all = BlogModel.objects.all().order_by('-created_date')
		elif(qd.__getitem__('category')):
			post_all = BlogModel.objects.filter(category=qd.__getitem__('category')).order_by('-created_date')
		# else:
		# 	post_all = BlogModel.objects.exclude(category=qd.__getitem__('category'))[:1].get()

	if("ALL"!=str(qd.__getitem__('category'))):
		# first_post = BlogModel.objects.filter(category=qd.__getitem__('category'))[:1].get()
		# post_all = BlogModel.objects.filter(category=qd.__getitem__('category')).exclude(blog_title=first_post.blog_title)
		post_all = BlogModel.objects.filter(category=qd.__getitem__('category')).order_by('-created_date')
	return render(request, "bloglisting.html", 
			{'post_all':post_all, 'seo_qurey_blog1':seo_qurey_blog1, 'seo_qurey_blog':seo_qurey_blog, 
			'category':category,'icon_qurey' : icon_qurey, 'copyright':copyright, 'footer':footer,
			'current_user':current_user, 'category_selected':qd.__getitem__('category'), })


	
def blog_detail_view(request, id=None):
	
	current_user = request.user
	current_post = get_object_or_404(BlogModel, id=id)
	
	# first_post = BlogModel.objects.all().first()
	first_post = BlogModel.objects.all()[:1].get()
	last_post = BlogModel.objects.all().last()
	post = BlogModel.objects.all()
	post_all = BlogModel.objects.exclude(id=1)
	category_all = BlogCategoryModel.objects.all()
	footer=Contact_Footernew.objects.all()
	copyright = CopyRightModel.objects.all()
	icon_qurey = Header_Icon_Model.objects.all()
	seo_qurey_blog = SEO_Model.objects.filter(seo_page="Blog")
	seo_qurey_blog1 = SEO_Model.objects.filter(seo_page="Blog")

	print (id)
	cat_post = BlogModel.objects.all()
	currrent_post = BlogModel.objects.filter(id=id)
	currrent_id = ""
	currrent_category = ""
	for i in currrent_post:
		print("a",i.id)
		currrent_id = i.id
		currrent_category = i.category
	if request.method == 'GET':
		qd = request.GET
		print ("\n\n\n",type(currrent_post))
		# print ("\n\n\nid",currrent_post("category"))
		cat_post =  BlogModel.objects.filter(category=currrent_category)
		# cat_id = []
		# for i in cat_post:
		# 	cat_id.append(i.id)
		# 	print (cat_id)
		# 	cat_id.sort()

		# cat_post[cat_post.index(category)+1]
		# print (cat_id)
	id_list = []
	for k in cat_post:
		id_list.append(k.id)


	#id_list contains all id of current category
	#sort id list
	previousitem = ""
	nextitem = ""
	id_list.sort()
	id_list_length = len(id_list)
	print (id_list_length)
	#next = next id or if next id is last in idlist then next = next
	
	#id = 1 =



	id = int(id)
	#id =0
	if id == id_list[0] and id_list_length>1:
		previousitem = id
		nextitem = id_list[id_list.index(id)+1]
		

	#id = 0 and only one record
	elif id == id_list[0] and id_list_length == 1:
		previousitem = id
		nextitem = id

	#id > 0 element and lenght != last
	elif id > id_list[0] and id_list_length != (id_list.index(id)+1):
		previousitem = int(id_list[id_list.index(id)-1])
		nextitem = int(id_list[id_list.index(id)+1])

	#id >0 element and length and length = last
	elif id > id_list[0] and id_list_length == (id_list.index(id)+1):
		previousitem = int(id_list[id_list.index(id)-1])
		nextitem = id

	
	# if id == id_list[id_list_length-1] and id <= a.id_list_length:
	# 	previousitem = id_list[id_list.index(id)-1]
	# 	nextitem = id_list[id_list_length-1]




	# if id <= id_list[id_list_length]:
	# 	nextitem = id
	# 	previous = id_list[id_list.index(id)-1]
	# 	print ("\n\n", nextitem)
	# 	print ("\n\n", previousitem)
	#id = maxt	

	#previs is prvis id if pres is presnent in idlist else it is zero

	

	return render(request, "blogcategory.html", 
			{'currrent_category':currrent_category,'current_post':current_post, 'post_all':post_all,'first_post':first_post, 'seo_qurey_blog1':seo_qurey_blog1, 'seo_qurey_blog':seo_qurey_blog, 
			'category':category_all,'icon_qurey' : icon_qurey, 'copyright':copyright, 'footer':footer, 'post':post,
			'current_user':current_user, "next":nextitem,"previous":previousitem})

@login_required(login_url='/login/')
def blog_post_admin(request):
	current_user = request.user
	post = BlogModel.objects.all()
	category = BlogCategoryModel.objects.all()
	copyright = CopyRightModel.objects.all()
	return render(request, "blog_admin.html", {'copyright':copyright, 'post':post, 'category':category, 'current_user':current_user,})

@login_required(login_url='/login/')
def blog_post_add(request):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	blog_form = BlogModelForm(request.POST or None, request.FILES or None)
	if blog_form.is_valid() and request.method == "POST" :
		instance = blog_form.save(commit=False)
		instance.save()
		return redirect('blog_post_admin')
	return render(request, 'blog_post_add.html', {'copyright':copyright, 'blog_form':blog_form, 'current_user' : current_user})

@login_required(login_url='/login/')
def blog_post_update(request, id=None):
	current_user = request.user
	copyright = CopyRightModel.objects.all()
	instance = get_object_or_404(BlogModel, id=id)
	blog_form = BlogModelForm(request.POST or None, request.FILES or None, instance=instance)
	if blog_form.is_valid() and request.method == "POST" :
		instance = blog_form.save(commit=False)
		instance.save()
		return redirect('blog_post_admin')
	return render(request, 'blog_post_update.html', {'copyright':copyright, 'blog_form':blog_form})

@login_required(login_url='/login/')
def blog_post_delete(request, id=None):
	current_user = request.user
	instance = get_object_or_404(BlogModel, id=id)
	instance.delete()
	return redirect('blog_post_admin')