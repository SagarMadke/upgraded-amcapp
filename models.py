from __future__ import unicode_literals
from django.db import models

from django.contrib.auth.models import User
#from django.contrib.admin.widgets import AdminDateWidget
from PIL import Image
from django.utils import timezone
from ckeditor.fields import RichTextField

CATEGORIES = [
	('Website','Website'),
	('Mobile','Mobile'),
	('IOS', 'IOS'),
	('Android', 'Android'),
	('Ecommerce','Ecommerce'),
	]

page = [
	('--select--', '--select--'),
	('Home', 'Home'),
	('Aboutus', 'Aboutus'),
	('Services', 'Services'),
	('HMS', 'HMS'),
	('Blog', 'Blog'),
	('Portfolio', 'Portfolio'),
	('Contactus', 'Contactus'),
	]



class ServicesHeaderModel(models.Model):
	header_title = models.CharField(max_length=255)
	header_bgimage = models.ImageField(upload_to="images/", blank = True)
	header_description = models.TextField()

	def __str__(self):
		return self.header_title

class ServicesGamePlanModel(models.Model):
	plan_title = models.CharField(max_length=255)
	plan_image = models.ImageField(upload_to="images/", blank = True)
	plan_description = models.TextField()

	def __str__(self):
		return self.plan_title

class ServicesFeaturesModel(models.Model):
	features_title = models.CharField(max_length=255)
	features_image = models.ImageField(upload_to="images/", blank = True)
	features_description = models.TextField()

	def __str__(self):
		return self.features_title

class ServicesOurServicesModel(models.Model):
	our_title = models.CharField(max_length=255)
	our_image = models.ImageField(upload_to="images/", blank = True)
	our_description = models.TextField()

	def __str__(self):
		return self.our_title

class ServicesPortfolioModel(models.Model):
	portfolio_title = models.CharField(max_length=255)
	portfolio_image = models.ImageField(upload_to="images/")
	portfolio_description = models.TextField()
	portfolio_url = models.CharField(max_length=255, blank=True)

	def __str__(self):
		return self.portfolio_title

class ContactUsModel(models.Model):
	con_title = models.CharField(max_length=255)
	con_icon_img = models.ImageField(upload_to="images/")
	con_desc1 = models.TextField()
	con_desc2 = models.TextField()

	def __str__(self):
		return self.con_title

class PortfolioCategoryModel(models.Model):
	category = models.CharField(max_length=255,unique=True)

	def __str__(self):
		return self.category




class PortfolioWebsiteModel(models.Model):
	web_icon = models.ImageField(upload_to="images/", blank=True)
	web_title = models.CharField(max_length=255)
	web_image = models.ImageField(upload_to="images/", blank=True)
	web_url = models.CharField(max_length=255)
	category = models.ForeignKey(PortfolioCategoryModel,to_field='category', default="--Select--")

	def __str__(self):
		return self.web_title



class PortfolioMobileModel(models.Model):
	mob_icon = models.ImageField(upload_to="images/", blank=True)
	mob_title = models.CharField(max_length=255)
	mob_image = models.ImageField(upload_to="images/", blank = True)
	mob_category = models.CharField(max_length=255, blank=True, choices=CATEGORIES)
	mob_url = models.CharField(max_length=255)

	def __str_(self):
		return self.mob_title



class ArticleModel(models.Model):
	art_image = models.ImageField(upload_to="images/", blank=True)
	art_date = models.CharField(max_length=255)
	art_month = models.CharField(max_length=255)
	art_calendar = models.DateTimeField(default=timezone.now)
	art_author = models.CharField(max_length= 500)
	art_title = models.CharField(max_length=500)
	art_description = models.TextField() 

	def __str__(self):
		return self.art_title

# --------------- Prashant Model---------------------
class About_us_HeaderImg(models.Model):
	Image = models.FileField(upload_to="uploads/")
	Title1 = models.TextField()
	Title2= models.TextField()

class About_us_Body(models.Model):
	Icon = models.FileField(upload_to="icons/")
	Title = models.CharField(max_length=200)
	Decription= models.TextField()

class About_us_Img(models.Model):
	Image = models.FileField(upload_to="icons/")
	Title = models.CharField(max_length=200)
	Decription= models.TextField()

class About_us_BodyImg(models.Model):
	Image = models.FileField(upload_to="uploads/")

class Portfolio_Page(models.Model):
	Icon = models.FileField(upload_to="icons/")
	Title = models.CharField(max_length=200)
	Image = models.FileField(upload_to="uploads/")
	

class Contact_Footernew(models.Model):
	country = models.CharField(max_length=800)
	adress_line1 = models.CharField(max_length=500)
	adress_line2 = models.CharField(max_length=500)
	state = models.CharField(max_length=500)
	pin = models.CharField(max_length=500)
	contact = models.CharField(max_length=500)

# ----------- Naresh --------------------------

class Top_image_icon(models.Model):
	image = models.ImageField(upload_to='photo/')
	text_title1 = models.CharField(max_length=8000)
	text_title2 = models.CharField(max_length=8000)

class Middle_image_icon(models.Model):
	icon1 = models.ImageField(upload_to='photo/')
	title1 = models.CharField(max_length=8000)
	discription = models.TextField(max_length=8000)

class Buttom_image_icon(models.Model):
	image_icon = models.ImageField(upload_to='photo/')
	icon2 = models.ImageField(upload_to='photo/')
	title2 = models.CharField(max_length=8000)
	image_url = models.CharField(max_length=500)

class Text_button(models.Model):
	txt = models.CharField(max_length=800)

# Here start Blog Page Model
class Blog_page(models.Model):
	blog_image = models.ImageField(upload_to='photo/')
	date_no = models.CharField(max_length=10)
	date_char = models.CharField(max_length=10)
	#calender1 = models.DateTimeField(max_length=100)
	calender = models.CharField(max_length=200)
	#timestamp = models.DateTimeField(auto_now_add= False, auto_now=True)
	#time_field = models.DateField(auto_now_add= True, auto_now=False)
	by = models.CharField(max_length=800)
	bolg_title = models.CharField(max_length=8000)
	bolg_discription = models.TextField(max_length=80000)

class Home_index_page1(models.Model):
	image_bg1 = models.ImageField(upload_to='photo/')
	title_index1 = models.CharField(max_length=8000)
	sub_title = models.CharField(max_length=8000)

class Home_index_page2(models.Model):
	icon_image1 = models.ImageField(upload_to='photo/')
	title_index2 = models.CharField(max_length=8000)
	dis_index1 = models.TextField(max_length=10000)


class Home_middle_page1(models.Model):
	image_bg2 = models.ImageField(upload_to='photo/')
	icon_image2 = models.ImageField(upload_to='photo/')
	title_index3 = models.CharField(max_length=8000)
	dis_index2 = models.TextField(max_length=8000)
	home_icon_url = models.CharField(max_length=50, default="www.amcits.sg")
	

class Home_middle_page2(models.Model):
	image_bg2p1 = models.ImageField(upload_to='photo/')
	icon_image2p2 = models.ImageField(upload_to='photo/')
	title_index3p3 = models.CharField(max_length=8000)
	dis_index2p4 = models.TextField(max_length=8000)
class Home_middle_page3(models.Model):
	image_bg2p5 = models.ImageField(upload_to='photo/')
	icon_image2p6 = models.ImageField(upload_to='photo/')
	title_index3p7 = models.CharField(max_length=8000)
	dis_index2p8 = models.TextField(max_length=8000)

class Home_index_page3(models.Model):
	image_bg3 = models.ImageField(upload_to='photo/')
	title_index4 = models.CharField(max_length=8000)

class Home_index_page4(models.Model):
	icon_image3 = models.ImageField(upload_to='photo/')
	title_index5 = models.CharField(max_length=8000)
	dis_index3 = models.TextField(max_length=10000)
	image_bottom = models.ImageField(upload_to='photo/')
		
	
	def get_absolute_url(self):
		    return "%s/" %(self.id)

# --------------- Footer - copyright ---------------

class CopyRightModel(models.Model):
	copy_right_quote = models.CharField(max_length=1000)

	def __str__(self):
		return self.copy_right_quote

# --------------- Social-media -Icons------------

class Header_Icon_Model(models.Model):
	icon_image = models.ImageField(upload_to='photo/')
	icon_title = models.CharField(max_length=200)
	icon_url = models.CharField(max_length=200)

	def __str__(self):
		return self.icon_title


#----------SEO Models-----------

class SEO_Model(models.Model):
	seo_title = models.CharField(max_length=1000)
	seo_discription = models.TextField(max_length=1000)
	seo_keyward  = models.CharField(max_length=1000)
	seo_page = models.CharField(max_length=1000, choices=page, default="--select--")

	def __str__(self):
		return self.seo_title


# ------------------------------------------------
class Blog(models.Model):
	author = models.ForeignKey('auth.User')
	# author = models.CharField(max_length=255)
	title =	models.CharField(max_length=255)
	text = models.TextField()

	created_date = models.DateTimeField(default=timezone.now)
	published_date = models.DateTimeField(blank=True, null=True)
	
	def	publish(self):
		self.published_date	= timezone.now()
		self.save()
	
	def	__str__(self):
		return	self.title

# ------------------------------------------------------
class BlogCategoryModel(models.Model):
	category = models.CharField(max_length=255,unique=True)

	def __str__(self):
		return self.category
# -----------------------------------------------------
class BlogModel(models.Model):
	# author = models.ForeignKey('auth.User')
	blog_title = models.CharField(max_length=255)
	blog_image = models.ImageField(upload_to="images/")
	# blog_description = models.TextField()
	blog_description = RichTextField(null=True,blank=True)
	created_date = models.DateTimeField(default=timezone.now)
	published_date = models.DateTimeField(blank=True, null=True)
	category = models.ForeignKey(BlogCategoryModel,to_field='category', default="--Select--")

	def	publish(self):
		self.published_date	= timezone.now()
		self.save()
	
	def	__str__(self):
		return	self.blog_title
# ---------------------------------------------------------
 